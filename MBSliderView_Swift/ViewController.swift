//
//  ViewController.swift
//  MBSliderView_Swift
//
//  Created by Muneeb Ahmed Anwar on 01/10/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var sliderFromStoryBoard : MBSliderView!
    var sliderFromCode = MBSliderView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // set delegate for sliderFromStoryBoard
        sliderFromStoryBoard.delegate = self
        
        // configure sliderFromCode
        sliderFromCode.frame = CGRect(x: 10, y: 300, width: self.view.frame.size.width - 20, height: 100)
        sliderFromCode.minValue = 0
        sliderFromCode.maxValue = 1
        sliderFromCode.currentValue = 3
        sliderFromCode.step = 2
        sliderFromCode.ignoreDecimals = true   // default value
        sliderFromCode.animateLabel = true      // default value
        sliderFromCode.delegate = self
        
        self.view.addSubview(sliderFromCode)
    }
}

extension ViewController: MBSliderDelegate {
    func sliderView(_ sliderView: MBSliderView, valueDidChange value: Float) {
        if sliderView == sliderFromStoryBoard {
            print("sliderFromStoryBoard: \(value)")
        } else if sliderView == sliderFromCode {
            print("sliderFromCode: \(value)")
        }
    }
}

